CREATE TABLE Admin (
  id SERIAL NOT NULL,
  email varchar(255),
  mdp varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE Caracteristique (
  id SERIAL NOT NULL,
  nom varchar(255),
  idUnite int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Client (
  id SERIAL NOT NULL,
  nom varchar(255),
  prenom varchar(255),
  email varchar(255),
  mdp varchar(255),
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE Mois (
  id SERIAL NOT NULL,
  nom varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE Piece (
  id SERIAL NOT NULL,
  nom varchar(255),
  description text,
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

CREATE TABLE PieceCaracteristique (
  id SERIAL NOT NULL,
  idPiece int4 NOT NULL,
  valeur varchar(255),
  idCaracteristique int4 NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE PieceImage (
  id SERIAL NOT NULL,
  idPiece int4 NOT NULL,
  image varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE StockPiece (
  id int4 NOT NULL,
  idPiece int4 NOT NULL,
  qte int4 DEFAULT 0,
  prix numeric(10, 2) DEFAULT 0,
  dateAjout timestamp DEFAULT now(),
  PRIMARY KEY (id, idPiece)
);

CREATE TABLE Unite (
  id SERIAL NOT NULL,
  nom varchar(255),
  PRIMARY KEY (id)
);

CREATE TABLE VenteClient (
  id SERIAL NOT NULL,
  idPiece int4 NOT NULL,
  idClient int4 NOT NULL,
  qte int4 DEFAULT 0,
  dateAjout timestamp DEFAULT now(),
  dateVente timestamp,
  etat int4 DEFAULT 0,
  PRIMARY KEY (id)
);

ALTER TABLE
  PieceImage
ADD
  CONSTRAINT FKPieceImage552170 FOREIGN KEY (idPiece) REFERENCES Piece (id);

ALTER TABLE
  Caracteristique
ADD
  CONSTRAINT FKCaracteris61225 FOREIGN KEY (idUnite) REFERENCES Unite (id);

ALTER TABLE
  PieceCaracteristique
ADD
  CONSTRAINT FKPieceCarac49899 FOREIGN KEY (idCaracteristique) REFERENCES Caracteristique (id);

ALTER TABLE
  PieceCaracteristique
ADD
  CONSTRAINT FKPieceCarac613383 FOREIGN KEY (idPiece) REFERENCES Piece (id);

ALTER TABLE
  StockPiece
ADD
  CONSTRAINT FKStockPiece104406 FOREIGN KEY (idPiece) REFERENCES Piece (id);

ALTER TABLE
  VenteClient
ADD
  CONSTRAINT FKVenteClien523788 FOREIGN KEY (idPiece) REFERENCES Piece (id);

ALTER TABLE
  VenteClient
ADD
  CONSTRAINT FKVenteClien475234 FOREIGN KEY (idClient) REFERENCES Client (id);
