create database panieradrware;

-- Install the extension (run this only once)
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- Now you can use the gen_salt function
INSERT INTO
  Admin (email, mdp)
VALUES
  (
    'admin@gmail.com',
    crypt('admin123', gen_salt('bf'))
  ),
  (
    'superadmin@gmail.com',
    crypt('super123', gen_salt('bf'))
  ),
  (
    'admin2@gmail.com',
    crypt('password123', gen_salt('bf'))
  );

INSERT INTO
  Client (nom, prenom, email, mdp, etat)
VALUES
  (
    'Dupont',
    'Jean',
    'jean@gmail.com',
    crypt('jean123', gen_salt('bf')),
    0
  ),
  (
    'Martin',
    'Marie',
    'marie@gmail.com',
    crypt('marie123', gen_salt('bf')),
    0
  ),
  (
    'Tremblay',
    'Pierre',
    'pierre@gmail.com',
    crypt('pierre123', gen_salt('bf')),
    0
  );

INSERT INTO
  Client (nom, prenom, email, mdp, etat)
VALUES
  (
    'Leblanc',
    'Sophie',
    'sophie@gmail.com',
    crypt('sophie123', gen_salt('bf')),
    0
  ),
  (
    'Gagnon',
    'Alexandre',
    'alexandre@gmail.com',
    crypt('alex123', gen_salt('bf')),
    0
  ),
  (
    'Lavoie',
    'Melissa',
    'melissa@gmail.com',
    crypt('melissa123', gen_salt('bf')),
    0
  ),
  (
    'Roy',
    'Mathieu',
    'mathieu@gmail.com',
    crypt('mathieu123', gen_salt('bf')),
    0
  ),
  (
    'Girard',
    'Isabelle',
    'isabelle@gmail.com',
    crypt('isabelle123', gen_salt('bf')),
    0
  );

INSERT INTO
  Mois (nom)
VALUES
  ('January'),
  ('February'),
  ('March'),
  ('April'),
  ('May'),
  ('June'),
  ('July'),
  ('August'),
  ('September'),
  ('October'),
  ('November'),
  ('December');