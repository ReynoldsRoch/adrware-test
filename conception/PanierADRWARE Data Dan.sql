-- Inserting data into Unite table
INSERT INTO
  Unite (nom)
VALUES
  ('Kilogramme'),
  ('Mètre'),
  ('Litre'),
  ('Pièce');

-- Inserting data into Caracteristique table
INSERT INTO
  Caracteristique (nom, idUnite)
VALUES
  ('Poids', 1),
  ('Longueur', 2),
  ('Capacité', 3),
  ('Matériel', 4);

-- Inserting data into Piece table
INSERT INTO
  Piece (nom, description, etat)
VALUES
  ('Roue', 'Roue de rechange pour voiture', 0),
  (
    'Pare-brise',
    'Pare-brise en verre pour la voiture',
    0
  ),
  ('Batterie', 'Batterie 12V pour la voiture', 0);

-- Inserting data into PieceCaracteristique table
INSERT INTO
  PieceCaracteristique (idPiece, valeur, idCaracteristique)
VALUES
  (1, '10', 1),
  (1, '0.6', 2),
  (1, '1', 4),
  (2, '5', 1),
  (2, '1.2', 2),
  (2, '1', 4),
  (3, '15', 1),
  (3, '0.3', 2),
  (3, '1', 4);