@extends('pages.layouts.app')

@section('title')
HOME
@endsection

@section('content')

<style>
    .grid-container {
        display: grid;
        grid-template-columns: repeat(2, 1fr);
        grid-template-rows: repeat(2, 1fr);
        gap: 5px;
    }

    .grid-item {
        /* padding: 20px; */
        /* font-size: 30px; */
        text-align: center;
        margin-bottom: 100px;
    }
</style>

<div class="row">

    <div class="col1 col-lg-4 col-md-0">

        <!-- Jumbotron -->
        <div class="bg-image p-5 text-center shadow-1-strong rounded mb-5 text-white" style="
        background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/003.webp');
        height: 100%;
        ">
            <div class="mask" style="background-color: rgba(0, 0, 0, 0.6);">
                <div class="d-flex justify-content-center align-items-center h-100">
                    <span class="text-white mb-0">
                        <h5>My Framework</h5>
                        <p class="text-white mb-0">
                            It always seems impossible, <br/>until it is done.
                        </p>
                    </span>
                </div>
            </div>
        </div>
        <!-- Jumbotron -->

    </div>

    <div class="col2 col-lg-8 col-md-12">
        <h2>RECENT PROJECTS</h2>
        <div class="grid-container">
            @if(session()->get('id_user') == null)
                @for ($i = 0; $i < 3; $i++)
                    <div class="grid-item">
                        <div class="card text-center" style="width: 70%;">
                            <div class="card-header">Dev Projects</div>
                            <div class="card-body">
                                <h5 class="card-title">Project n°{{$i}}</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                            </div>
                            <div class="card-footer text-muted">2 days ago</div>
                        </div>
                    </div>
                @endfor
            @else
                @foreach($projets as $projet)
                    <div class="grid-item">
                        <div class="card text-center" style="width: 70%;">
                            <div class="card-header">Project</div>
                            <div class="card-body">
                                <h5 class="card-title">{{$projet->nom}}</h5>
                                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                <a href="/project/{{$projet->id}}"><button type="button" class="btn btn-primary">Check</button></a>
                            </div>
                            <div class="card-footer text-muted">Last update : {{$projet->update_at}}</div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>

</div>

@endsection