<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        REGISTER
    </title>

    <!-- Favicons -->

    <!-- MDB icon -->
    <link rel="icon" href="{{ asset('bootstrap-md5/img/mdb-favicon.ico') }}" type="image/x-icon" />

    <!-- MDB -->
    <link rel="stylesheet" href="{{ asset('bootstrap-md5/css/mdb.min.css') }}" />


</head>

<body>
    <div class="container">

        <div class="row">

            <!-- Image and text -->
            <nav class="navbar navbar-light bg-light">
                <div class="container-fluid">
                    <a class="navbar-brand mt-2 mt-lg-0" href="#">
                        <small>
                            REGISTER
                        </small>
                    </a>

                    <!-- <a href="\"><button type="button" class="btn btn-info">Back home</button></a> -->

                </div>
            </nav>

            <div class="col-lg-7 col-md-6">
                <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp" class="img-fluid" alt="Sample image">
            </div>

            <div class="col-lg-5 col-md-6">

                <!-- Pills navs -->
                <ul class="nav nav-pills nav-justified mb-3" id="ex1" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="tab-login" data-mdb-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true">Login</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="tab-register" data-mdb-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false">Register</a>
                    </li>
                </ul>
                <!-- Pills navs -->

                <!-- Pills content -->
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="tab-login">
                        <form id='login-form'>
                            @csrf
                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" value="john.smith@example.com" id="loginName" class="form-control" name="email" required />
                                <label class="form-label" for="loginName">Email</label>
                            </div>

                            <!-- Password input -->
                            <div class="form-outline mb-4">
                                <input type="password" value="password123" id="loginPassword" class="form-control" name="password" required />
                                <label class="form-label" for="loginPassword">Password</label>
                            </div>

                            <!-- 2 column grid layout -->
                            <div class="row mb-4">
                                <div class="col-md-6 d-flex justify-content-center">
                                    <!-- Checkbox -->
                                    <div class="form-check mb-3 mb-md-0">
                                        <input class="form-check-input" type="checkbox" value="" id="loginCheck" checked />
                                        <label class="form-check-label" for="loginCheck"> Remember me </label>
                                    </div>
                                </div>

                                <div class="col-md-6 d-flex justify-content-center">
                                    <!-- Simple link -->
                                    <a href="#">Forgot password?</a>
                                </div>
                            </div>

                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>

                        </form>
                    </div>
                    <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="tab-register">
                        <form id="sign-up-form">
                            <!-- <form onsubmit="checkSignUp()"> -->
                            @csrf
                            <!-- Name input -->
                            <div class="form-outline mb-4">
                                <input type="text" value="Leon Scott" id="registerName" class="form-control" name="nom" required />
                                <label class="form-label" for="registerName">Name</label>
                            </div>

                            <!-- Surname input -->
                            <div class="form-outline mb-4">
                                <input type="text" value="Kennedy" id="registerUsername" class="form-control" name="prenom" required />
                                <label class="form-label" for="registerUsername">Surname</label>
                            </div>


                            <!-- Birthday input -->
                            <div class="form-outline mb-4">
                                <input type="date" value="1997-04-04" id="registerBirthday" class="form-control" name="dtn" required />
                                <label class="form-label" for="registerBirthday">Birthday</label>
                            </div>

                            <!-- Email input -->
                            <div class="form-outline mb-4">
                                <input type="email" value="leon.scott@gmail.com" id="registerEmail" class="form-control" name="email" required />
                                <label class="form-label" for="registerEmail">Email</label>
                            </div>

                            <!-- Password input -->
                            <div class="form-outline mb-4">
                                <input type="password" value="resident" id="registerPassword" class="form-control" name="password" required />
                                <label class="form-label" for="registerPassword">Password</label>
                            </div>

                            <!-- Repeat Password input -->
                            <div class="form-outline mb-4">
                                <input type="password" value="resident" id="registerRepeatPassword" class="form-control" name="confirm" required />
                                <label class="form-label" for="registerRepeatPassword">Repeat password</label>
                            </div>

                            <!-- Checkbox -->
                            <div class="form-check d-flex justify-content-center mb-4">
                                <input class="form-check-input me-2" type="checkbox" value="" id="registerCheck" checked aria-describedby="registerCheckHelpText" />
                                <label class="form-check-label" for="registerCheck">
                                    I have read and agree to the terms
                                </label>
                            </div>

                            <!-- Submit button -->
                            <button type="submit" class="btn btn-primary btn-block mb-3">Sign in</button>
                        </form>
                    </div>
                </div>
                <!-- Pills content -->

            </div>

        </div>

        <div class="row">
            <center>
                <hr>
                <footer>
                    <p>Copyright &copy; PLANIO | Designed by <a href="#">ETU001455</a> | 2023.</p>
                </footer>
            </center>


        </div>


    </div>

    <!-- Script -->
    <script src="{{ asset('js-projects/app.js') }}"></script>

    <!-- MDB -->
    <script type="text/javascript" src="{{ asset('bootstrap-md5/js/mdb.min.js') }}"></script>
    <!--  -->
    <script src="{{ asset('bootstrap-5/js/jquery.js') }}"></script>
    <script src="{{ asset('bootstrap-5/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('bootstrap-5/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script src="{{ asset('js/sweetAlert.js') }}"></script>
</body>

</html>