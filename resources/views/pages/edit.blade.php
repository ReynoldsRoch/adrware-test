@extends('pages.layouts.app')

@section('title')
EDIT
@endsection

@section('content')
<div class="row">
    <center>
        <form id='login-form'>
            @csrf
            <!-- Email input -->
            <div class="form-outline mb-4">
                <input type="email" value="john.smith@example.com" id="loginName" class="form-control" name="email" required />
                <label class="form-label" for="loginName">Email</label>
            </div>

            <!-- Password input -->
            <div class="form-outline mb-4">
                <input type="password" value="password123" id="loginPassword" class="form-control" name="password" required />
                <label class="form-label" for="loginPassword">Password</label>
            </div>

            <!-- Submit button -->
            <button type="submit" class="btn btn-success btn-block mb-4">EDIT</button>
            
        </form>
        <button type="submit" class="btn btn-danger btn-block mb-4">DELETE</button>
    </center>
</div>
@endsection