<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
</head>

<body>

    <center>

        <h1>{{$title}}</h1>

        <hr>

        <table border="1" style="border-collapse: collapse;">
            <tr>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Dtn</th>
            </tr>

            @foreach($clients as $client)
            <tr>
                <td>{{$client->nom}}</td>
                <td>{{$client->prenom}}</td>
                <td>{{$client->dtn}}</td>
            </tr>
            @endforeach
        </table>

    </center>

</body>

</html>