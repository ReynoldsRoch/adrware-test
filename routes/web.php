<?php

use App\Http\Controllers\FrameworkController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\MyMailController;
use App\Http\Controllers\PdfController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Reynolds' modif

Route::get('download-pdf',[
    PdfController::class,
    'downloadPdf'
]);

Route::get('generate-pdf',[
    PdfController::class,
    'generatePdf'
]);

Route::get('edit',[
    FrameworkController::class,
    'edit'
]);

Route::get('client-article',[
    FrameworkController::class,
    'getClientArticle'
]);

Route::get('dbName',[
    FrameworkController::class,
    'getDbName'
]);

Route::get('list',[
    FrameworkController::class,
    'list'
]);

// Auto-complete
Route::get('autoComplete',[
    homeController::class,
    'autoComplete'
])->name('autoComplete');

// Email

Route::get('sendEmail', [
    MyMailController::class,
    'sendEmail'
]);

// 

Route::get('/log-out', [
    homeController::class,
    'logOut']
);


Route::get('/createTask', [
    homeController::class,
    'createTask'
]);

Route::post('/createProject', [
    homeController::class,
    'createProject'
]);


// 

Route::get('/', [homeController::class, 'home']);
Route::get('/login', [homeController::class, 'login'])->name('login');
Route::post('/authentify', [homeController::class, 'authentify']);
Route::post('/insert', [homeController::class, 'insert']);
Route::get('/project/{id}', [homeController::class, 'project'])->name('project');
Route::get('/myproject', [homeController::class, 'myproject'])->name('myproject');
