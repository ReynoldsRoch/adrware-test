<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Article extends Model
{
    use HasFactory;
    protected $table = 'article';
    public $timestamps = false;

    public function client(): BelongsTo
    {
        # code...
        return $this->belongsTo(Client::class, 'idclient');
    }

    public function images(): HasMany
    {
        # code...
        return $this->hasMany(ArticleImage::class, 'idarticle');
    }
}
