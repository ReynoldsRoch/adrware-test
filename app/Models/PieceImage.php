<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PieceImage extends Model
{
    use HasFactory;
    protected $table = 'pieceimage';
    public $timestamps = false;
}
