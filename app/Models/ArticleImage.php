<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ArticleImage extends Model
{
    use HasFactory;
    protected $table = 'articleimage';
    public $timestamps = false;

    public function article(): BelongsTo
    {
        # code...
        return $this->belongsTo(Article::class, 'idarticle');
    }
}
