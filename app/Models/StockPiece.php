<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockPiece extends Model
{
    use HasFactory;
    protected $table = 'stockpiece';
    public $timestamps = false;
}
