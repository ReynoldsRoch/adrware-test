<?php

namespace App\Http\Controllers;

use App\Mail\MyMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MyMailController extends Controller
{
    //
    public function sendEmail($receiver = null)
    {
        # code...
        ini_set('max_execution_time', 120);

        $header = 'Register Successfully!';
        $body = 'Thank you for signing in Planio. Hope we will help you.';

        $details = [
            'title' => 'PLANIO REGISTER',
            'header' => $header,
            'body' => $body,
        ];

        $receiverName = 'reynoroch@gmail.com';

        if ($receiver != null) {
            # code...
            $receiverName = $receiver;
        }

        // Mail::to('ariniaina18@gmail.com')->send(new MyMail($details));

        // Mail::to('anjarandriamanalina@gmail.com')->send(new MyMail($details));
        Mail::to($receiverName)->send(new MyMail($details));

        // return "Email sent successfully!";
    }
}
