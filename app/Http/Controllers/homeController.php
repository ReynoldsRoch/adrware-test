<?php

namespace App\Http\Controllers;

use App\Mail\MyMail;
use App\Models\Projet;
use App\Models\Tache;
use App\Models\TypeTache;
use App\Models\Typetodo;
use App\Models\Utilisateur;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class homeController extends Controller
{

    // Reynolds' modif

    public function autoComplete(Request $request)
    {
        # code...
        $query = $request->get('query');
        $id = session()->get('id_user');

        // echo $id;

        // echo $query;

        $utilisateur = DB::table('utilisateur')
            ->where('nom', 'LIKE', '%' . $query . '%')
            ->orWhere('prenom', 'LIKE', '%' . $query . '%')
            ->whereNot('id', $id)
            ->get();

        // dd($utilisateur);

        // return json_encode($utilisateur);
        return response()->json($utilisateur);
    }

    public function logOut()
    {
        # code...
        // session()->forget('key'); // Replace 'key' with the name of your session variable
        // Alternatively, you can use session()->flush() to remove all session data

        // session()->flush();
        session()->forget('id_user');

        // Redirect to the login page or other appropriate page
        return redirect()->route('login');
    }

    public function createTask(Request $request)
    {
        # code...
        $tache = new Tache();
        $tache->idprojet = $request['idProjet'];
        $tache->nom = $request['taskName'];
        $tache->description = $request['taskDescription'];
        $tache->datedebut = $request['dateDebut'] . " " . $request['timeDebut'];
        $tache->datefin = $request['dateFin'] . " " . $request['timeFin'];

        // TypeTache default Work
        $tache->setIdTypeTache();
        // echo $tache->idtypetache;

        // Priority 0
        $tache->priorite = 0;

        // Reminder
        $tache->setReminder($request['reminder']);
        // echo "reminder : " . $tache->reminder . "<br/>";

        // Etat tache
        $tache->setEtatTache();
        // echo $tache->idetat;

        $tache->save();

        // return redirect()->route('project/'.$tache->idprojet);
        return redirect()->route('project', ['id' => $tache->idprojet]);
    }

    public function createProject(Request $request)
    {
        # code...

        $projet = new Projet();
        $projet->nom = $request['projectName'];
        // echo $projet->nom;
        $projet->idtypetodo = $request['typeProject'];
        // echo $projet->idtypetodo;
        $projet->idutilisateur = session()->get('id_user');
        // echo $projet->idtypeutilisateur;
        $projet->datecreation = Carbon::now()->format("Y-m-d");
        $projet->update_at = Carbon::now();
        // echo $projet->update_at;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . ' - ' . $image->getClientOriginalName();
            $projet->img = $imageName;
            $image->move(public_path('img-projects/uploads'), $imageName);
            // echo $imageName;
        } else {
            // Handle case where no file was uploaded
        }

        $projet->save();

        return redirect()->route('myproject');
    }

    public function myproject()
    {
        # code...
        $typetodo = Typetodo::all();
        $data = [
            'typeToDo' => $typetodo
        ];

        if (session()->get('id_user') != null) {
            $id_user = session()->get('id_user');
            $user = Utilisateur::find($id_user);
            $projetsPerso = ($user->projets)->where('idtypetodo', 1);
            $projetsGroup = ($user->projets)->where('idtypetodo', 2);
            $data['projetsPerso'] = $projetsPerso;
            $data['projetsGroup'] = $projetsGroup;
        }
        return view('pages.myproject', $data);
    }

    public function project($id)
    {
        $projet = Projet::find($id);

        $projet->update_at = Carbon::now();
        $projet->update();

        $not_started = 0;
        $on_going = 0;
        $avancement = 0;

        if (count(($projet->taches)) != 0) {
            # code...
            $not_started = count(($projet->taches)->where('idetat', 1)) / count(($projet->taches));
            $on_going = count(($projet->taches)->where('idetat', 2)) / count(($projet->taches));
            $avancement = count(($projet->taches)->where('idetat', 3)) / count(($projet->taches));
        }


        $data = [
            'taches_a_faire' => ($projet->taches)->where('idetat', 1),
            'taches_en_cours' => ($projet->taches)->where('idetat', 2),
            'taches_finies' => ($projet->taches)->where('idetat', 3),

            'projet' => $projet,
            'avancement' => $avancement * 100,
            'not_started' => $not_started * 100,
            'on_going' => $on_going * 100
        ];
        // Je filtre les tâches par état en les stockant dans différentes collections
        return view('pages.project', $data);
    }


    // 

    public function home(): View
    {
        $dbname = DB::connection()->getDatabaseName();
        // echo "Connected database name is: {$dbname}";

        if (session()->get('id_user') != null) {
            $id_user = session()->get('id_user');
            $user = Utilisateur::find($id_user);
            $projets = $user->projets;
            $data = [
                'projets' => $projets->sortByDesc('update_at')->values()->take(3),
            ];
            return view('pages.home', $data);
        }
        return view('pages.home');
    }

    public function login(): View
    {
        if (session()->get('id_user') != null)
            session()->forget('id_user');
        return view('pages.login');
    }

    public function authentify(Request $request)
    {
        $user = Utilisateur::log($request);
        if (is_null($user))
            return 0;
        else {
            session()->put('id_user', $user->id);
            return 1;
        }
    }

    public function insert(Request $request)
    {
        $utilisateur = Utilisateur::create(
            [
                'nom' => $request['nom'],
                'prenom' => $request['prenom'],

                // control if age > 16
                'datenaissance' => $request['dtn'],

                'email' => $request['email'],
                'password' => md5($request['password']),
            ]
        );
        if ($utilisateur->save()) {
            // send a validation email
            // $this->sendEmail($utilisateur->email);
            return 1;
        } else
            return 0;
    }

    public function sendEmail($receiver = null)
    {
        # code...
        ini_set('max_execution_time', 120);

        $header = 'Register Successfully!';
        $body = 'Thank you for signing in Planio. Hope we will help you.';

        $details = [
            'title' => 'PLANIO REGISTER',
            'header' => $header,
            'body' => $body,
        ];

        $receiverName = 'reynoroch@gmail.com';

        if ($receiver != null) {
            # code...
            $receiverName = $receiver;
        }

        // Mail::to('ariniaina18@gmail.com')->send(new MyMail($details));

        // Mail::to('anjarandriamanalina@gmail.com')->send(new MyMail($details));

        Mail::to($receiverName)->send(new MyMail($details));

        // return "Email sent successfully!";
    }
}
