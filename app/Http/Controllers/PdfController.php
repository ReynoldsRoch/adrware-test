<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Http\Request;
use Dompdf\Dompdf;

class PdfController extends Controller
{
    // Version 2
    public function generatePdf()
    {
        # code...
        // Pdf content
        $data = [
            'title' => 'PDF TITLE',
            'clients' => Client::all()
        ];

        $html = view('pdf.example', $data)->render();

        // echo $html;

        $pdf = new Dompdf();

        // Load HTML content
        $pdf->loadHtml($html);

        // // Set the title of the PDF document
        // $pdf->getOptions()->set('title', 'My PDF Document');

        // // Render the PDF document
        $pdf->render();

        // // Get the current directory path
        $directoryPath = public_path('pdfs\\'); // Change the path to your desired folder

        // // Set the output file path and name
        $outputFilePath = $directoryPath . 'my-pdf-document-3.pdf';

        // echo $outputFilePath . "<br/>";

        // // Save the PDF document to the specified folder
        // $pdf->stream($outputFilePath);

        $pdf->stream('my-pdf-document-3.pdf');

        echo "PDF saved successfully!";
    }

    // Version 1
    /*
    public function generatePdf()
    {
        # code...
        // Pdf content
        $content = "<center><h1>MY PDF LARAVEL</h1><center><hr/>";

        $content .= "<h2>My First Pdf</h2>";
        $content .= "<p>Hello Laravel Pdf.</p>";

        $content .= "<center>
            <hr>
            <footer>
                <h5>&copy; ReynoldsLaravel | Designed by <a href=\"#\">ETU001455</a> | 2023.</h5>
            </footer>
        </center>";

        $pdf = new Dompdf();

        // Set the title of the PDF document
        $pdf->getOptions()->set('title', 'My PDF Document');

        // Render the PDF document
        try {
            //code...
            $pdf->render();
        } catch (\Exception  $e) {
            //throw $th;
            echo "Exception : " . $e;
        }

        // Get the current directory path
        $directoryPath = public_path('pdfs/'); // Change the path to your desired folder

        // Set the output file path and name
        $outputFilePath = $directoryPath . 'my-pdf-document.pdf';

        // Save the PDF document to the specified folder
        file_put_contents($outputFilePath, $pdf->output());

        echo "PDF saved successfully!";
    }
    */
}
